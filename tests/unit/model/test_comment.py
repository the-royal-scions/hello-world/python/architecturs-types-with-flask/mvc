import re
import unittest
from app import create_app
from app.extensions.database import db
from app.model.comments import CommentsModel


class CommentsModelTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- Comments Model Test Case Started ----------")
        pass

    def setUp(self):
        print("\n")
        self.app = create_app('test')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

        self.data = {"comment": "Test Coment"}
        self.comment = CommentsModel(**self.data)

    def test_comment(self):
        print("Testing comment attribute")
        self.assertEqual(
            self.comment.comment, self.data["comment"]
        )

    def test_uuid4(self):
        print("Testing uuid attribute")
        self.assertEqual(len(self.comment.uuid), 36)
        matched = re.match(
            r"\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b",
            self.comment.uuid)
        self.assertTrue(bool(matched))

    def test_model_persistence(self):
        print("Testing model persistence")
        self.comment.save()
        all_comments = CommentsModel.all()
        new_comment = all_comments[0]
        self.assertEqual(len(all_comments), 1)
        self.assertTrue(new_comment == self.comment)

        self.comment.delete()
        all_comments = CommentsModel.all()
        self.assertEqual(len(all_comments), 0)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- Comments Model Test Case Finished ----------")
        print("#######################################################")
        pass
