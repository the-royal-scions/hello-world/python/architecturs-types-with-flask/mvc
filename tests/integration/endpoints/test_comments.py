import unittest
from app import create_app


class APICommentsTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- Comments Endpoint ('/api/comments') Test Case Started ----------")
        pass

    def setUp(self):
        print("\n")
        self.app = create_app("test")
        self.app_client = self.app.test_client()
        self.app_client.testing = True
        self.response = self.app_client.get("/api/comments")
        self.response_json = self.response.get_json()

    def test_get(self):
        print("Testing get status code")
        self.assertEqual(200, self.response.status_code)

    def test_response_content_type(self):
        print("Testing get content type")
        self.assertIn("application/json", self.response.content_type)

    def test_response_content(self):
        print("Testing get content")
        self.assertTrue(isinstance(self.response_json, list))

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- Comments Endpoint Test Case Finished ----------")
        print("#######################################################")
        pass
