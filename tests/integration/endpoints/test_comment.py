import unittest
from app import create_app
from app.model.comments import CommentsModel
from app.serealizer.comments import CommentsSchema

comment_schema = CommentsSchema()


class APICommentTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- Comment Endpoint ('/api/comment') Test Case Started ----------")
        cls.app = create_app("test")
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        cls.app.db.create_all()
        cls.app_client = cls.app.test_client()
        cls.app_client.testing = True

        cls.data = {"comment": "Test Coment"}
        cls.comment = CommentsModel(**cls.data)
        cls.comment.save()

        cls.data_to_delete_method = {"comment": "Test Delete"}
        cls.comment_to_delete_method = CommentsModel(**cls.data)
        cls.comment_to_delete_method.save()

    def setUp(self):
        print("\n")
        pass

    def test_get_method(self):
        print("Testing get method")
        url = "/api/comment"
        uuid = self.comment.uuid
        query_string = {"uuid": uuid}
        self.response = self.app_client.get(url, query_string=query_string)

        self.assertEqual(200, self.response.status_code)

        self.assertIn("application/json", self.response.content_type)

        response_json = self.response.get_json()

        comment_data = comment_schema.load(response_json)
        self.assertEqual(comment_data["uuid"], self.comment.uuid)
        self.assertEqual(comment_data["comment"], self.comment.comment)

    def test_post_method(self):
        print("Testing post method")
        url = "/api/comment"
        json = {"comment": "New Test Comment"}
        self.response = self.app_client.post(url, json=json)

        self.assertEqual(201, self.response.status_code)

        self.assertIn("application/json", self.response.content_type)

        response_json = self.response.get_json()

        comment_data = comment_schema.load(response_json)
        comment = CommentsModel.filter_by(uuid=comment_data["uuid"])
        self.assertTrue(comment)

        comment.delete()
        comment = CommentsModel.filter_by(uuid=comment.uuid)
        self.assertFalse(comment)

    def test_delete_method(self):
        print("Testing delete method")
        url = "/api/comment"
        uuid = self.comment_to_delete_method.uuid
        query_string = {"uuid": uuid}
        self.response = self.app_client.delete(url, query_string=query_string)

        self.assertEqual(200, self.response.status_code)

        self.assertIn("application/json", self.response.content_type)

        comment = CommentsModel.filter_by(uuid=uuid)
        self.assertFalse(comment)

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- CommentsEndpoint Test Case Finished ----------")
        print("#######################################################")
        pass
