import unittest
from app import create_app


class IndexTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("\n")
        print("#######################################################")
        print("---------- Index Endpoint ('/') Test Case Started ----------")
        pass

    def setUp(self):
        print("\n")
        self.app = create_app("test")
        self.app_client = self.app.test_client()
        self.app_client.testing = True
        self.response = self.app_client.get("/")

    def test_get(self):
        print("Testing get status code")
        self.assertEqual(200, self.response.status_code)

    def test_response_content_type(self):
        print("Testing get content type")
        self.assertIn("text/html", self.response.content_type)

    def test_response_content(self):
        print("Checking for gitlab link")
        response_str = self.response.data.decode("utf-8")
        gitlab_link =\
            '<a href="https://gitlab.com/the-royal-scions/hello-world/python/clean_flask">'
        self.assertIn(gitlab_link, str(response_str))

    def test_bootstrap(self):
        print("Checking for bootstrap link")
        response_str = self.response.data.decode("utf-8")
        self.assertIn("bootstrap.min.css", response_str)
        self.assertIn("bootstrap.min.js", response_str)

    def test_jquery(self):
        print("Checking for jquery link")
        response_str = self.response.data.decode("utf-8")
        self.assertIn("jquery-3.5.1.min.js", response_str)

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        print("\n")
        print("---------- Index Endpoint Test Case Finished ----------")
        print("#######################################################")
        pass
