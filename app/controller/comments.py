from flask import jsonify
from flask.views import MethodView
from app.model.comments import CommentsModel
from app.serealizer.comments import CommentsSchema

comments_schema = CommentsSchema(many=True)


class CommentsResource(MethodView):
    @classmethod
    def get(cls):
        """
        Get all comments in database
        ---
        tags:
            - comments
        responses:
          200:
            description: Success in get comments 
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
         """
        comments = CommentsModel.all()
        response = comments_schema.dump(comments)
        return jsonify(response), 200
