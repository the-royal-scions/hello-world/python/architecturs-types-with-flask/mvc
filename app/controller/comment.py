from flask import request, jsonify
from flask.views import MethodView
from app.model.comments import CommentsModel
from app.serealizer.comments import CommentsSchema

comment_schema = CommentsSchema()


class CommentResource(MethodView):
    @classmethod
    def get(cls):
        """
        Get comment by uuid
        ---
        parameters:
          - in: query
            name: uuid
            type: string
            required: true
        responses:
          200:
            description: Success to get the comment
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """
        uuid = request.args.get("uuid")
        if not uuid:
            return {"message": "Missing query string param 'uuid'"}, 400

        comment_data = comment_schema.load(
            {"uuid": uuid}, partial=("comment",))

        comment = CommentsModel.filter_by(uuid=comment_data["uuid"])
        if not comment:
            return {"message": "Comment not found"}, 400

        response = comment_schema.dump(comment)
        return jsonify(response), 200

    @classmethod
    def post(cls):
        """
        Create a comment
        ---
        consumes:
            - application/json
        parameters:
          - in: body
            name: comment
            schema:
              type: object
              required:
                - comment
              properties:
                comment:
                  type: string
        responses:
          200:
            description: Success to create the comment
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """

        request_json = request.get_json()
        if not request_json:
            return {"message": "A body with json is expected"}, 400

        comment_data = comment_schema.load(
            request_json, partial=("uuid",))
        if not comment_data:
            return {"message": "Missing comment on json object"}, 400

        try:
            comment = CommentsModel.create(**comment_data)
            response = comment_schema.dump(comment)
            return jsonify(response), 201
        except:
            return {"message": "Internal server error"}, 500

    @classmethod
    def put(cls):
        """
        Update a comment by uuid
        ---
        consumes:
            - application/json
        parameters:
          - in: body
            name: comment
            schema:
              type: object
              required:
                - uuid
                  comment
              properties:
                uuid:
                  type: string
                comment:
                  type: string
        responses:
          200:
            description: Success to update the comment
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """

        request_json = request.get_json()
        if not request_json:
            return {"message": "A body with json is expected"}, 400

        comment_data = comment_schema.load(request_json)

        comment = CommentsModel.filter_by(uuid=comment_data["uuid"])
        if not comment:
            return {"message": "Comment not found"}, 400

        try:
            CommentsModel.create(comment=comment_data["comment"])
            comment.delete()
            return {"message": "Comment updated"}, 200
        except:
            return {"message": "Internal server error"}, 500

    @classmethod
    def patch(cls):
        """
        Update a comment  by uuid
        ---
        consumes:
            - application/json
        parameters:
          - in: body
            name: comment
            schema:
              type: object
              required:
                - uuid
                  comment
              properties:
                uuid:
                  type: string
                comment:
                  type: string
        responses:
          200:
            description: Success to update the comment
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """

        request_json = request.get_json()
        if not request_json:
            return {"message": "A body with json is expected"}, 400

        comment_data = comment_schema.load(request_json)

        comment = CommentsModel.filter_by(uuid=comment_data["uuid"])
        if not comment:
            return {"message": "Comment not found"}, 400

        try:
            comment.update(comment=comment_data["comment"])
            return {"message": "Comment updated"}, 200
        except:
            return {"message": "Internal server error"}, 500

    @classmethod
    def delete(cls):
        """
        Delete a comment by uuid
        ---
        parameters:
          - in: query
            name: uuid
            type: string
            required: true
        responses:
          200:
            description: Success to delete a comment by uuid
            schema:
              id: CommentsModel
              properties:
                uuid:
                  type: string
                  description: Unique identifier of the comment
                comment:
                  type: string
                  description: Comment it self
        """
        uuid = request.args.get("uuid")
        if not uuid:
            return {"message": "Missing query string param 'uuid'"}, 400

        comment = CommentsModel.filter_by(uuid=uuid)
        if not comment:
            return {"message": "Comment not found"}, 400

        try:
            comment.delete()
            return {"message": "Comment deleted"}, 200
        except:
            return {"message": "Internal server error"}, 500
