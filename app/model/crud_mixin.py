from app.extensions.database import db


class CRUDMixin(object):

    __table_args__ = {'extend_existing': True}

    @classmethod
    def create(cls, **kwargs) -> object:
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit=True, **kwargs) -> object:
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True) -> object:
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True) -> None:
        db.session.delete(self)
        return commit and db.session.commit()

    @classmethod
    def filter_by(cls, **kwargs) -> object:
        return cls.query.filter_by(**kwargs).first()

    @classmethod
    def all(cls) -> object:
        return cls.query.all()
