from flask import Blueprint
from .index import index
from .errors import register_error_handler

bp = Blueprint("view", __name__,
               template_folder="templates",
               static_folder="static")

register_error_handler(bp)

bp.add_url_rule("/", view_func=index)


def init_view(app):
    app.register_blueprint(bp)
