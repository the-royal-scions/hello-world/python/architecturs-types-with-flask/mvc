from app.extensions.database import db
from app.model.comments import CommentsModel
from app.serealizer.comments import CommentsSchema


def register_shell_context(app):
    def shell_context():
        return {"CommentsModel": CommentsModel,
                "CommentsSchema": CommentsSchema,
                "db": db}

    app.shell_context_processor(shell_context)
