from app.extensions.serealizer import ma
from app.model.comments import CommentsModel


class CommentsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CommentsModel
